const Router = require('express').Router();

const {
    ServerError
} = require('./errors');

const {
    checkUserIntervals,
    checkIntervals,
    makeReservation
} = require('./services.js');

Router.get('/check/my', async (req, res) => {

    const {
        username
    } = req.body;
    
    const intervals = await checkUserIntervals(username);

    res.json(intervals);
});

Router.get('/check/', async (req, res) => {

    const {
        username
    } = req.body;
    
    const intervals = await checkIntervals(username);

    res.json(intervals);
});

Router.post('/', async (req, res) => {

    const {
        username,
        startdate,
        duration
    } = req.body;

    if (!username) {
        throw new ServerError('No title provided!', 400);
    }

    if (!startdate) {
        throw new ServerError('No author provided!', 400);
    }

    if (!duration) {
        throw new ServerError('No genre provided!', 400);
    }

    const id = await makeReservation(username, startdate, duration);

    res.json(id);
});

module.exports = Router;