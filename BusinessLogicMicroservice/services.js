const {
    sendRequest
} = require('./http-client');

const checkUserIntervals = async (username) => {
    console.info(`Sending request to IO for all reservations for user ${username} ...`);
    
    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/reservations/user`,
        method: 'GET',
        data: {
            username
        }
    }

    const books = await sendRequest(options);

    return books;
};

const checkIntervals = async (username) => {
    console.info(`Sending request to IO for all reservations for user ${username} ...`);
    
    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/reservations`,
        method: 'GET',
        data: {
            username
        }
    }

    const books = await sendRequest(options);

    return books;
};

const makeReservation = async (username, startdate, duration) => {
    console.info(`Sending request to IO to add reservation for user ${username}, start-date ${startdate} and duration ${duration} ...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/reservations`,
        method: 'POST',
        data: {
            username,
            startdate,
            duration
        }
    }

    const id = await sendRequest(options);

    return id;
};  

module.exports = {
    checkUserIntervals,
    checkIntervals,
    makeReservation
}