
CREATE SCHEMA idp_rezervari;
CREATE TABLE users (
	id_user serial PRIMARY KEY,
	username varchar(25) NOT NULL,
	password varchar(25) NOT NULL,
	login varchar(25) NOT NULL,
	data_login varchar(40),
	no_rezervari_active integer);


CREATE TABLE IF NOT EXISTS intervale (
	id_interval integer NOT NULL PRIMARY KEY,
	interv_8_9 integer,
	interv_9_10 integer,
	interv_10_11 integer,
	interv_11_12 integer,
	interv_12_13 integer,
	interv_13_14 integer,
	interv_14_15 integer,
	interv_15_16 integer,
	interv_16_17 integer,
	interv_17_18 integer,
	interv_18_19 integer,
	interv_19_20 integer
);

CREATE TABLE IF NOT EXISTS zile_disponibile (
	id_zi integer NOT NULL PRIMARY KEY,
	id_interval_disp integer NOT NULL, 
	data_zi varchar(40) NOT NULL,
	capacitate_interval int NOT NULL,
	FOREIGN KEY(id_interval_disp) REFERENCES intervale(id_interval)
);

CREATE TABLE IF NOT EXISTS rezervare_sala (
	id_rezervare serial PRIMARY KEY,
	id_user integer NOT NULL,
	data_rezervare varchar(40) NOT NULL,
	durata_rezervare integer NOT NULL,
	id_zi integer NOT NULL,
	slot_rezervare integer NOT NULL,
	FOREIGN KEY(id_user) REFERENCES users(id_user),
	FOREIGN KEY(id_zi) REFERENCES zile_disponibile(id_zi)
);

INSERT INTO intervale (id_interval, interv_8_9, interv_9_10, interv_10_11, interv_11_12, interv_12_13,
					  interv_13_14, interv_14_15, interv_15_16, interv_16_17, interv_17_18, interv_18_19, interv_19_20)
					  VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

INSERT INTO intervale (id_interval, interv_8_9, interv_9_10, interv_10_11, interv_11_12, interv_12_13,
					  interv_13_14, interv_14_15, interv_15_16, interv_16_17, interv_17_18, interv_18_19, interv_19_20)
					  VALUES (2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

INSERT INTO intervale (id_interval, interv_8_9, interv_9_10, interv_10_11, interv_11_12, interv_12_13,
					  interv_13_14, interv_14_15, interv_15_16, interv_16_17, interv_17_18, interv_18_19, interv_19_20)
					  VALUES (3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

INSERT INTO intervale (id_interval, interv_8_9, interv_9_10, interv_10_11, interv_11_12, interv_12_13,
					  interv_13_14, interv_14_15, interv_15_16, interv_16_17, interv_17_18, interv_18_19, interv_19_20)
					  VALUES (4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

INSERT INTO intervale (id_interval, interv_8_9, interv_9_10, interv_10_11, interv_11_12, interv_12_13,
					  interv_13_14, interv_14_15, interv_15_16, interv_16_17, interv_17_18, interv_18_19, interv_19_20)
					  VALUES (5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

INSERT INTO intervale (id_interval, interv_8_9, interv_9_10, interv_10_11, interv_11_12, interv_12_13,
					  interv_13_14, interv_14_15, interv_15_16, interv_16_17, interv_17_18, interv_18_19, interv_19_20)
					  VALUES (6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

INSERT INTO intervale (id_interval, interv_8_9, interv_9_10, interv_10_11, interv_11_12, interv_12_13,
					  interv_13_14, interv_14_15, interv_15_16, interv_16_17, interv_17_18, interv_18_19, interv_19_20)
					  VALUES (7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

INSERT INTO zile_disponibile (id_zi, id_interval_disp, data_zi, capacitate_interval) 
	VALUES (1, 1, CAST(CURRENT_DATE + INTERVAL '0 day' AS VARCHAR), 20);

INSERT INTO zile_disponibile (id_zi, id_interval_disp, data_zi, capacitate_interval) 
	VALUES (2, 2, CAST(CURRENT_DATE + INTERVAL '1 day' AS VARCHAR), 20);

INSERT INTO zile_disponibile (id_zi, id_interval_disp, data_zi, capacitate_interval) 
	VALUES (3, 3, CAST(CURRENT_DATE + INTERVAL '2 day' AS VARCHAR), 20);

INSERT INTO zile_disponibile (id_zi, id_interval_disp, data_zi, capacitate_interval) 
	VALUES (4, 4, CAST(CURRENT_DATE + INTERVAL '3 day' AS VARCHAR), 20);

INSERT INTO zile_disponibile (id_zi, id_interval_disp, data_zi, capacitate_interval) 
	VALUES (5, 5, CAST(CURRENT_DATE + INTERVAL '4 day' AS VARCHAR), 20);

INSERT INTO zile_disponibile (id_zi, id_interval_disp, data_zi, capacitate_interval) 
	VALUES (6, 6, CAST(CURRENT_DATE + INTERVAL '5 day' AS VARCHAR), 20);

INSERT INTO zile_disponibile (id_zi, id_interval_disp, data_zi, capacitate_interval) 
	VALUES (7, 7, CAST(CURRENT_DATE + INTERVAL '6 day' AS VARCHAR), 20);
