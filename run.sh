cd ./AuthMicroservice/
docker build -t dennismircea/auth-service .
cd ..

cd ./BusinessLogicMicroservice/
docker build -t dennismircea/businesslogic-service .
cd ..

cd ./IOMicroservice/
docker build -t dennismircea/io-service .
cd ..