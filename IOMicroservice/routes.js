const Router = require('express').Router();

const IOController = require('./reservations/controllers.js');

Router.use('/reservations', IOController);

module.exports = Router;