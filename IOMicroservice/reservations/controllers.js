const Router = require('express').Router();

const {
    getUserReservations,
    getReservations,
    addReservation
} = require('./services.js');

Router.get('/user', async (req, res) => {

    const {
        username
    } = req.body;
    
    const reservations = await getUserReservations(username);

    res.json(reservations);
});

// route to see the free intervals
Router.get('/', async (req, res) => {

    const {
        username
    } = req.body;
    
    const reservations = await getReservations(username);

    res.json(reservations);
});

Router.post('/', async (req, res) => {

    const {
        username,
        startdate,
        duration
    } = req.body;

    const id = await addReservation(username, startdate, duration);

    res.json(id);
});

module.exports = Router;