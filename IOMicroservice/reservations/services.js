const {
    query
} = require('../data');

const verifyTimeLapse = async (username) => {
    var today = new Date();

    const logdate = await query("SELECT data_login FROM users u WHERE u.username=$1", [username]);
    var userdate = new Date(logdate[0].data_login);

    if ((today.getTime() - userdate.getTime()) / (1000 * 3600) < 2) {
        return 0;
    }

    return -1;
}

const verifyDayPassed = async () => {
    var today = new Date();

    const days = await query("SELECT * FROM zile_disponibile");

    var i = 0;
    while (days[i]) {
        date_day = new Date(days[i].data_zi);
        if ((today.getTime() - date_day.getTime()) / (1000 * 3600 * 24) > 1) {
            await query("UPDATE zile_disponibile SET data_login=CAST($1 + INTERVAL '7 day' AS VARCHAR) WHERE id_zi=$2", [date_day, days[i].id_zi]);
        }
        i += 1;
    }
    return 0;
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    return [year, month, day].join('-') + ' 00:00:00';
}

const increment_interval = async (hour, id_zi) => {
    var ret = 0;
    switch (hour) {
        case 8:
            var val_interv = await query("SELECT interv_8_9 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_8_9 += 1;
            await query("UPDATE intervale SET interv_8_9=$1 WHERE id_interval=$2", [val_interv[0].interv_8_9, id_zi]);
            ret = 8;
            break;
        case 9:
            var val_interv = await query("SELECT interv_9_10 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_9_10 += 1;
            await query("UPDATE intervale SET interv_9_10=$1 WHERE id_interval=$2", [val_interv[0].interv_9_10, id_zi]);
            ret = 9;
            break;
        case 10:
            var val_interv = await query("SELECT interv_10_11 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_10_11 += 1;
            await query("UPDATE intervale SET interv_10_11=$1 WHERE id_interval=$2", [val_interv[0].interv_10_11, id_zi]);
            ret = 10;
            break;
        case 11:
            var val_interv = await query("SELECT interv_11_12 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_11_12 += 1;
            await query("UPDATE intervale SET interv_11_12=$1 WHERE id_interval=$2", [val_interv[0].interv_11_12, id_zi]);
            ret = 11;
            break;
        case 12:
            var val_interv = await query("SELECT interv_12_13 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_12_13 += 1;
            await query("UPDATE intervale SET interv_12_13=$1 WHERE id_interval=$2", [val_interv[0].interv_12_13, id_zi]);
            ret = 12;
            break;
        case 13:
            var val_interv = await query("SELECT interv_13_14 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_13_14 += 1;
            await query("UPDATE intervale SET interv_13_14=$1 WHERE id_interval=$2", [val_interv[0].interv_13_14, id_zi]);
            ret = 13;
            break;
        case 14:
            var val_interv = await query("SELECT interv_14_15 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_14_15 += 1;
            await query("UPDATE intervale SET interv_14_15=$1 WHERE id_interval=$2", [val_interv[0].interv_14_15, id_zi]);
            ret = 14;
            break;
        case 15:
            var val_interv = await query("SELECT interv_15_16 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_15_16 += 1;
            await query("UPDATE intervale SET interv_15_16=$1 WHERE id_interval=$2", [val_interv[0].interv_15_16, id_zi]);
            ret = 15;
            break;
        case 16:
            var val_interv = await query("SELECT interv_16_17 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_16_17 += 1;
            await query("UPDATE intervale SET interv_16_17=$1 WHERE id_interval=$2", [val_interv[0].interv_16_17, id_zi]);
            ret = 16;
            break;
        case 17:
            var val_interv = await query("SELECT interv_17_18 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_17_18 += 1;
            await query("UPDATE intervale SET interv_17_18=$1 WHERE id_interval=$2", [val_interv[0].interv_17_18, id_zi]);
            ret = 17;
            break;
        case 18:
            var val_interv = await query("SELECT interv_18_19 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_18_19 += 1;
            await query("UPDATE intervale SET interv_18_19=$1 WHERE id_interval=$2", [val_interv[0].interv_18_19, id_zi]);
            ret = 18;
            break;
        case 19:
            var val_interv = await query("SELECT interv_19_20 FROM intervale WHERE id_interval=$1", [id_zi]);
            val_interv[0].interv_19_20 += 1;
            await query("UPDATE intervale SET interv_19_20=$1 WHERE id_interval=$2", [val_interv[0].interv_19_20, id_zi]);
            ret = 19;
            break;
        default:
            return `Interval rezervare depasit`;
    }
    return ret;
}

const checkCapacity = async (slot, max_capacity, id_zi) => {
    var val_interv;
    var cap = 0;
    switch (slot) {
        case 8:
            val_interv = await query("SELECT interv_8_9 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_8_9;
            break;
        case 9:
            val_interv = await query("SELECT interv_9_10 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_9_10;
            break;
        case 10:
            val_interv = await query("SELECT interv_10_11 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_10_11;
            break;
        case 11:
            val_interv = await query("SELECT interv_11_12 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_11_12;
            break;
        case 12:
            val_interv = await query("SELECT interv_12_13 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_12_13;
            break;
        case 13:
            val_interv = await query("SELECT interv_13_14 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_13_14;
            break;
        case 14:
            val_interv = await query("SELECT interv_14_15 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_14_15;
            break;
        case 15:
            val_interv = await query("SELECT interv_15_16 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_15_16;
            break;
        case 16:
            val_interv = await query("SELECT interv_16_17 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_16_17;
            break;
        case 17:
            val_interv = await query("SELECT interv_17_18 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_17_18;
            break;
        case 18:
            val_interv = await query("SELECT interv_18_19 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_18_19;
            break;
        case 19:
            val_interv = await query("SELECT interv_19_20 FROM intervale WHERE id_interval=$1", [id_zi]);
            cap = val_interv[0].interv_19_20;
            break;
        default:
            return -1;
    }
    if (cap > max_capacity) {
        return -1;
    }
    return 0;
}

const getUserReservations = async (username) => {
    console.info(`Getting all intervals for reservations for user ${username} ...`);

    const is_logged = await verifyTimeLapse(username);

    if (is_logged == -1) {
        return `The user ${username} is not logged`;
    }

    console.info(`The user ${username} is logged`);

    const id_user = await query("SELECT id_user FROM users u WHERE u.username=$1", [username]);

    console.info(`The id user is ${id_user[0].id_user}`);

    const user_res = await query("SELECT * FROM rezervare_sala WHERE id_user=$1", [id_user[0].id_user]);
    const count = await query("SELECT count(id_rezervare) FROM rezervare_sala WHERE id_user=$1", [id_user[0].id_user]);

    console.info(count[0].count);
    var string_res = 'The user ' + username + ' have ' + count[0].count + ' reservations:' + '\
    ';
    var i = 0;
    while (user_res[i]) {
        string_res += '     ' + i + ': At the ' + user_res[i].data_rezervare + ' with duration ' + user_res[i].durata_rezervare + '\
        ';
        i += 1;
    }

    if (i == 0) {
        return `No reservations for user ${username}`;
    }

    return string_res;
};

const getReservations = async (username) => {
    console.info(`Getting all intervals for reservations for user ${username} ...`);

    const is_logged = await verifyTimeLapse(username);

    if (is_logged == -1) {
        return `The user ${username} is not logged`;
    }

    const free_days = await query("SELECT * FROM zile_disponibile");
    var i = 0;
    var string_res = '';
    while (free_days[i]) {
        const intervals = await query("SELECT * FROM intervale WHERE id_interval=$1", [free_days[i].id_zi]);

        if (intervals[0].interv_8_9 > 0) {
            string_res += 'There are ' +  intervals[0].interv_8_9 + ' reservations in interval 8-9 on ' + free_days[i].data_zi;
        }
        if (intervals[0].interv_9_10 > 0) {
            string_res += 'There are ' +  intervals[0].interv_9_10 + ' reservations in interval 9-10 on ' + free_days[i].data_zi;
        }
        if (intervals[0].interv_10_11 > 0) {
            string_res += 'There are ' +  intervals[0].interv_10_11 + ' reservations in interval 10-11 on ' + free_days[i].data_zi;
        }
        if (intervals[0].interv_11_12 > 0) {
            string_res += 'There are ' +  intervals[0].interv_11_12 + ' reservations in interval 11-12 on ' + free_days[i].data_zi;
        }
        if (intervals[0].interv_12_13 > 0) {
            string_res += 'There are ' +  intervals[0].interv_12_13 + ' reservations in interval 12-13 on ' + free_days[i].data_zi;
        }
        if (intervals[0].interv_13_14 > 0) {
            string_res += 'There are ' +  intervals[0].interv_13_14 + ' reservations in interval 13-14 on ' + free_days[i].data_zi;
        }
        if (intervals[0].interv_14_15 > 0) {
            string_res += 'There are ' +  intervals[0].interv_14_15 + ' reservations in interval 14-15 on ' + free_days[i].data_zi;
        }
        if (intervals[0].interv_15_16 > 0) {
            string_res += 'There are ' +  intervals[0].interv_15_16 + ' reservations in interval 15-16 on ' + free_days[i].data_zi;
        }
        if (intervals[0].interv_16_17 > 0) {
            string_res += 'There are ' +  intervals[0].interv_16_17 + ' reservations in interval 16-17 on ' + free_days[i].data_zi;
        }
        if (intervals[0].interv_17_18 > 0) {
            string_res += 'There are ' +  intervals[0].interv_17_18 + ' reservations in interval 17-18 on ' + free_days[i].data_zi;
        }
        if (intervals[0].interv_18_19 > 0) {
            string_res += 'There are ' +  intervals[0].interv_18_19 + ' reservations in interval 18-19 on ' + free_days[i].data_zi;
        }
        if (intervals[0].interv_19_20 > 0) {
            string_res += 'There are ' +  intervals[0].interv_19_20 + ' reservations in interval 19-20 on ' + free_days[i].data_zi;
        }
        i += 1;
    }

    const count = await query("SELECT count(id_rezervare) FROM rezervare_sala");

    if (count[0].count == 0) {
        return `No reservations`;
    }
    
    return string_res;
};

const addReservation = async (username, startdate, duration) => {
    console.info(`Adding reservation for user ${username}, start-date ${startdate} and duration ${duration} ...`);

    // check pass day
    await verifyDayPassed();

    const is_logged = await verifyTimeLapse(username);

    if (is_logged == -1) {
        return `The user ${username} is not logged`;
    }

    console.info(`The user ${username} is logged`);

    var data_rezervare = new Date(startdate);
    var today = new Date();

    if (!data_rezervare) {
        return `The reservation data must be formatted like: DDD, Day_Number MMM YYYY hh:mm`;
    }

    if ((data_rezervare.getTime() - today.getTime()) / (1000 * 3600 * 24) > 6) {
        return `A reservation cannot be done at more than 7 days.`;
    }

    var m = data_rezervare.getMinutes();

    if (m != 0) {
        return `The data must be at an exact hour, like 8:00, 9:00 ...`;
    }

    const id_user = await query("SELECT id_user FROM users u WHERE u.username=$1", [username]);

    const data_zi = formatDate(data_rezervare);
    const id_zi = await query("SELECT id_zi FROM zile_disponibile z WHERE z.data_zi=$1", [data_zi]);

    // check the user reservations
    const user_res = await query("SELECT * FROM rezervare_sala WHERE id_user=$1", [id_user[0].id_user]);

    var i = 0;
    while (user_res[i]) {
        var d = new Date(user_res[i].data_rezervare);
        if (d.getTime() - data_rezervare.getTime() == 0) {
            return `You are not allowed to make more than 1 reservation in one interval or at the same date.`;
        }
        i += 1;
    }

    // check capacity
    const cap = await query("SELECT capacitate_interval FROM zile_disponibile z WHERE z.data_zi=$1", [data_zi]);
    const ok_cap = await checkCapacity(parseInt(data_rezervare.getHours()), cap[0].capacitate_interval, id_zi[0].id_zi);
    if (ok_cap == -1) {
        return `The capacity in the required interval is full. Please select other interval.`;
    }

    // Update intervals
    const slot = await increment_interval(parseInt(data_rezervare.getHours()), id_zi[0].id_zi);

    var id_user_val = id_user[0].id_user;
    var id_zi_val = id_zi[0].id_zi;

    await query("INSERT INTO rezervare_sala (id_user, data_rezervare, durata_rezervare, id_zi, slot_rezervare) VALUES ($1, $2, $3, $4, $5) RETURNING id_rezervare",
                        [id_user_val, data_rezervare, duration, id_zi_val, slot]);

    return `The reservation was made successfully!`;
};  

module.exports = {
    getUserReservations,
    getReservations,
    addReservation
}