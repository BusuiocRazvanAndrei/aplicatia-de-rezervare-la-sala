const Router = require('express').Router();

const AuthController = require('./auth/controllers.js');

Router.use('/auth', AuthController);

module.exports = Router;