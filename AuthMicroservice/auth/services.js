const {
    query
} = require('../data');

const Register = async (username, password) => {
    const users = await query(`SELECT username FROM users`);

    var pos_found = -1;
    for (var i = 0 in users) {
        if (users[i].username === username) {
            pos_found = i;
            break;
        }
    }

    if (pos_found === -1) {
        console.info(`Register the user ${username}`);

        const regist = await query("INSERT INTO users (username, password, login, no_rezervari_active) VALUES ($1, $2, $3, $4) RETURNING id_user",
                        [username, password, "False", 0]);

        return `Succesfully registered!`;
    }

    return `The user ${username} is already registered`;
};

const LogIn = async (username, password) => {

    const users = await query(`SELECT username FROM users`);

    var pos_found = -1;
    for (var i = 0 in users) {
        if (users[i].username === username) {
            pos_found = i;
            break;
        }
    }

    if (pos_found >= 0) {
        console.info(`Log in the user ${username}`);

        const logpass = await query("SELECT password FROM users WHERE username = $1", [username]);

        if (logpass[0].password != password) {
            return `Incorrect password for user ${username}`;
        }

        var today = new Date().toISOString().slice(0, 19).replace('T', ' ');

        const login = await query("UPDATE users SET data_login=$1, login=True WHERE username=$2 and password=$3", [today, username, password]);

        return `Succesfully logged in!`;
    }

    return `The user ${username} is not registered`;
};

module.exports = {
    Register,
    LogIn
}