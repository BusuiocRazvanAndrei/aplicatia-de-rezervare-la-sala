const Router = require('express').Router();

const {
    Register,
    LogIn
} = require('./services.js');

Router.post('/register/', async (req, res) => {

    const {
        username,
        password
    } = req.body;

    if (!username) {
        throw new ServerError('No username provided!', 400);
    }

    if (!password) {
        throw new ServerError('No password provided!', 400);
    }

    const id = await Register(username, password);

    res.json(id);
});

Router.post('/', async (req, res) => {

    const {
        username,
        password
    } = req.body;

    if (!username) {
        throw new ServerError('No username provided!', 400);
    }

    if (!password) {
        throw new ServerError('No password provided!', 400);
    }

    const id = await LogIn(username, password);

    res.json(id);
});

module.exports = Router;